# KEYBINDINGS {{{

bindkey -v
KEYTIMEOUT=1

bindkey '^f' vi-cmd-mode

# }}}


# HISTORY {{{

HISTFILE=$ZDOTDIR/histfile
HISTSIZE=1000
SAVEHIST=1000
setopt histignorealldups
setopt appendhistory
setopt incappendhistory

alias hvi="vi $HISTFILE"
alias hcat="cat $HISTFILE"
alias hgrep="hcat | grep"

# }}}


# PROMPT APPEARANCE {{{

function set-prompt () {
    case ${KEYMAP} in
      (vicmd)      VI_MODE="$(echo -n "-- NORMAL --")" ;;
      (main|viins) VI_MODE="$(echo -n "-- INSERT --")" ;;
      (*)          VI_MODE="$(echo -n "-- INSERT --")" ;;
    esac

    cu="%{%(!.%B%F{160}.%B%F{221})%}"
    cw="%B%F{15}"
    sl=" |-"
    sr="-| "
    sm=" "
    vm="%{$terminfo_sc$VI_MODE$terminfo[r]%}"

    PS1="$cw$sr$cu%n@%m$cw$sl$sm$sr$cu$vm$cw$sl$sm$sr$cu%/$cw$sl
$sr$cu%#%f%b "
}

function zle-line-init zle-keymap-select {
    set-prompt
    zle reset-prompt
}
# preexec () { print -rn -- $terminfo[el]; }

zle -N zle-line-init
zle -N zle-keymap-select

# }}}


# GENERAL ALIASES {{{

#ls
alias ls="ls --color"
alias ltr="ls -ltr"

#make
alias amek="make"
alias amke="make"
alias maek="make"
alias mkae="make"

#mutt
alias mutt="neomutt"

#.out, .err, .res
alias ct='cat  $(ls -t *.out | head -1)'
alias vt='vim  $(ls -t *.out | head -1)'
alias tt='tail $(ls -t *.out | head -1)'
alias ht='head $(ls -t *.out | head -1)'
alias cr='cat  $(ls -t *.err | head -1)'
alias vr='vim  $(ls -t *.err | head -1)'
alias tr='tail $(ls -t *.err | head -1)'
alias hr='head $(ls -t *.err | head -1)'
alias cs='cat  $(ls -t *.res | head -1)'
alias vs='vim  $(ls -t *.res | head -1)'
alias ts='tail $(ls -t *.res | head -1)'
alias hs='head $(ls -t *.res | head -1)'

# }}}


# SSH {{{

function ssh-unlock-key()
{
eval $(ssh-agent) > /dev/null

expect << EOF > /dev/null
    spawn ssh-add
    expect "Enter passphrase"
    send "$(pass $(hostname))\r"
    expect eof
EOF

$@
}

alias ssh="ssh-unlock-key /usr/bin/ssh"
alias sshfs="ssh-unlock-key /usr/bin/sshfs"
alias scp="ssh-unlock-key /usr/bin/scp"
alias rsync="ssh-unlock-key /usr/bin/rsync"
alias git="ssh-unlock-key /usr/bin/git"
alias dotfiles="ssh-unlock-key dotfiles"

# }}}


# PATH {{{

export PATH=$HOME/bin:$PATH

# }}}


# MISCELLANEOUS {{{

#SYNTAX HIGHLIGHTING
. /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#ZSH_HIGHLIGHT_STYLES[globbing]='fg=yellow'
#ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=yellow'

#Colours in man
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m"   ) \
        LESS_TERMCAP_md=$(printf "\e[1;31m"   ) \
        LESS_TERMCAP_me=$(printf "\e[0m"      ) \
        LESS_TERMCAP_se=$(printf "\e[0m"      ) \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m"      ) \
        LESS_TERMCAP_us=$(printf "\e[1;32m"   ) \
            man "$@"
}

# }}}
