# Completion
c.fonts.completion.category      = 'bold 8pt monospace'
c.fonts.completion.entry         = '8pt monospace'

# Messages
c.fonts.messages.error           = '8pt monospace'
c.fonts.messages.info            = '8pt monospace'
c.fonts.messages.warning         = '8pt monospace'

# Web
c.fonts.web.family.cursive       = ''
c.fonts.web.family.fantasy       = ''
c.fonts.web.family.fixed         = ''
c.fonts.web.family.sans_serif    = ''
c.fonts.web.family.serif         = ''
c.fonts.web.family.standard      = ''
c.fonts.web.size.default         = 16
c.fonts.web.size.default_fixed   = 13
c.fonts.web.size.minimum         = 0
c.fonts.web.size.minimum_logical = 6

# Misc
c.fonts.debug_console            = '8pt monospace'
c.fonts.downloads                = '8pt monospace'
c.fonts.hints                    = 'bold 10pt monospace'
c.fonts.keyhint                  = '8pt monospace'
c.fonts.prompts                  = '8pt sans-serif'
c.fonts.statusbar                = '8pt monospace'
