# Levels
c.zoom.default       = '100%'
c.zoom.levels        = [  '25%',  '33%',  '50%',  '67%',  '75%',  '90%', '100%'
                       , '110%', '125%', '150%', '175%', '200%', '250%', '300%'
                       , '400%', '500%']

# Misc
c.zoom.mouse_divider = 512
c.zoom.text_only     = False
