import sys, os

config.load_autoconfig(False)

config.source('config/colors.py')
config.source('config/bind.py')
config.source('config/fonts.py')
config.source('config/zoom.py')

searchengine='https://duckduckgo.org/'

#sys.path.append(os.path.join(sys.path[0], 'jblock'))
#config.source("jblock/jblock/integrations/qutebrowser.py")

c.aliases = {'w': 'session-save', 'q': 'quit', 'wq': 'quit --save'}

# How often (in milliseconds) to auto-save config/cookies/etc.
# Type: Int
c.auto_save.interval = 15000

# Always restore open sites when qutebrowser is reopened.
# Type: Bool
c.auto_save.session = False

c.backend = 'webengine'


# How many commands to save in the command history. 0: no history / -1:
# unlimited
# Type: Int
c.completion.cmd_history_max_items = 0

# The height of the completion, in px or as percentage of the window.
# Type: PercOrInt
c.completion.height = '50%'

# Move on to the next part when there's only one possible completion
# left.
# Type: Bool
c.completion.quick = True

# Padding of scrollbar handle in the completion window (in px).
# Type: Int
c.completion.scrollbar.padding = 2

# Width of the scrollbar in the completion window (in px).
# Type: Int
c.completion.scrollbar.width = 12

# When to show the autocompletion window.
# Type: String
# Valid values:
#   - always: Whenever a completion is available.
#   - auto: Whenever a completion is requested.
#   - never: Never.
c.completion.show = 'always'

# Shrink the completion to be smaller than the configured size if there
# are no scrollbars.
# Type: Bool
c.completion.shrink = False

# How to format timestamps (e.g. for the history completion).
# Type: TimestampTemplate
c.completion.timestamp_format = '%Y-%m-%d'

# How many URLs to show in the web history. 0: no history / -1:
# unlimited
# Type: Int
c.completion.web_history.max_items = 0

# Whether quitting the application requires a confirmation.
# Type: ConfirmQuit
# Valid values:
#   - always: Always show a confirmation.
#   - multiple-tabs: Show a confirmation if multiple tabs are opened.
#   - downloads: Show a confirmation if downloads are running
#   - never: Never show a confirmation.
c.confirm_quit = ['never']

# Whether support for the HTML 5 web application cache feature is
# enabled. An application cache acts like an HTTP cache in some sense.
# For documents that use the application cache via JavaScript, the
# loader engine will first ask the application cache for the contents,
# before hitting the network.
# Type: Bool
c.content.cache.appcache = True

# The maximum number of pages to hold in the global memory page cache.
# The Page Cache allows for a nicer user experience when navigating
# forth or back to pages in the forward/back history, by pausing and
# resuming up to _n_ pages. For more information about the feature,
# please refer to: http://webkit.org/blog/427/webkit-page-cache-i-the-
# basics/
# Type: Int
c.content.cache.maximum_pages = 0

# Size of the HTTP network cache. Null to use the default value.
# Type: Int
c.content.cache.size = None

# Control which cookies to accept.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain.
#   - never: Don't accept cookies at all.
c.content.cookies.accept = 'no-3rdparty'

# Store cookies. Note this option needs a restart with QtWebEngine on Qt
# < 5.9.
# Type: Bool
c.content.cookies.store = False

# Default encoding to use for websites. The encoding must be a string
# describing an encoding such as _utf-8_, _iso-8859-1_, etc.
# Type: String
c.content.default_encoding = 'iso-8859-1'

# Try to pre-fetch DNS entries to speed up browsing.
# Type: Bool
c.content.dns_prefetch = True

# Expand each subframe to its contents. This will flatten all the frames
# to become one scrollable page.
# Type: Bool
c.content.frame_flattening = False

# Allow websites to request geolocations.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.geolocation = 'ask'

# Value to send in the `Accept-Language` header.
# Type: String
c.content.headers.accept_language = 'en-US,en'

# Set custom headers for qutebrowser HTTP requests.
# Type: Dict
c.content.headers.custom = {}

# Value to send in the `DNT` header. When this is set to true,
# qutebrowser asks websites to not track your identity. If set to null,
# the DNT header is not sent at all.
# Type: Bool
c.content.headers.do_not_track = True

# Send the Referer header. The Referer header tells websites from which
# website you were coming from when visting them.
# Type: String
# Valid values:
#   - always: Always send the Referer.
#   - never: Never send the Referer. This is not recommended, as some sites may break.
#   - same-domain: Only send the Referer for the same domain. This will still protect your privacy, but shouldn't break any sites.
c.content.headers.referer = 'same-domain'

# User agent to send. Unset to send the default.
# Type: String
c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0'

# Whether host blocking is enabled.
# Type: Bool
c.content.blocking.enabled = True

# List of URLs of lists which contain hosts to block.  The file can be
# in one of the following formats:  - An `/etc/hosts`-like file - One
# host per line - A zip-file of any of the above, with either only one
# file, or a file named   `hosts` (with any extension).
# Type: List of Url
#c.content.blocking.hosts.lists = [ 'https://www.malwaredomainlist.com/hostslist/hosts.txt'
#                                 , 'http://someonewhocares.org/hosts/hosts'
#                                 , 'http://winhelp2002.mvps.org/hosts.zip'
#                                 , 'http://malwaredomains.lehigh.edu/files/justdomains.zip'
#                                 , 'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext'
#                                 , 'http://pasted.co/b2dc3032/fullscreen.php?hash=109b8d3e5696585ce16b62be141257c4&toolbar=true&linenum=false'
#                                 ]
c.content.blocking.hosts.lists = ['https://easylist.to/easylist/easylist.txt'
                                   , 'https://easylist.to/easylist/easyprivacy.txt'
                                   , 'https://easylist-downloads.adblockplus.org/easylistdutch.txt'
                                   , 'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt'
                                   , 'https://www.i-dont-care-about-cookies.eu/abp/'
                                   , 'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt'
                                   ]

# List of domains that should always be loaded, despite being ad-
# blocked. Domains may contain * and ? wildcards and are otherwise
# required to exactly match the requested domain. Local domains are
# always exempt from hostblocking.
# Type: List of String
c.content.blocking.whitelist = ['piwik.org']

# Enable or disable hyperlink auditing (`<a ping>`).
# Type: Bool
c.content.hyperlink_auditing = False

# Whether images are automatically loaded in web pages.
# Type: Bool
c.content.images = True

# Show javascript alerts.
# Type: Bool
c.content.javascript.alert = True

# Whether JavaScript can read from or write to the clipboard. With
# QtWebEngine, writing the clipboard as response to a user interaction
# is always allowed.
# Type: Bool
c.content.javascript.can_access_clipboard = False

# Whether JavaScript can close tabs.
# Type: Bool
c.content.javascript.can_close_tabs = False

# Whether JavaScript can open new tabs without user interaction.
# Type: Bool
c.content.javascript.can_open_tabs_automatically = False

# Enables or disables JavaScript.
# Type: Bool
c.content.javascript.enabled = True

# Log levels to use for JavaScript console logging messages. When a
# JavaScript message with the level given in the dictionary key is
# logged, the corresponding dictionary value selects the qutebrowser
# logger to use. On QtWebKit, the "unknown" setting is always used.
# Type: Dict
c.content.javascript.log = {'info': 'debug', 'unknown': 'debug', 'warning': 'debug', 'error': 'debug'}

# Use the standard JavaScript modal dialog for `alert()` and `confirm()`
# Type: Bool
c.content.javascript.modal_dialog = False

# Show javascript prompts.
# Type: Bool
c.content.javascript.prompt = True

# Whether locally loaded documents are allowed to access other local
# urls.
# Type: Bool
c.content.local_content_can_access_file_urls = True

# Whether locally loaded documents are allowed to access remote urls.
# Type: Bool
c.content.local_content_can_access_remote_urls = False

# Whether support for HTML 5 local storage and Web SQL is enabled.
# Type: Bool
c.content.local_storage = True

# Location of a netrc-file for HTTP authentication. If unset, `~/.netrc`
# is used.
# Type: File
c.content.netrc_file = None

# Allow websites to show notifications.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
# c.content.notifications = 'ask'

# Enable pdf.js to view PDF files in the browser. Note that the files
# can still be downloaded by clicking the download button in the pdf.js
# viewer.
# Type: Bool
c.content.pdfjs = False

# Enables or disables plugins in Web pages.
# Type: Bool
c.content.plugins = False

# Whether the background color and images are also drawn when the page
# is printed.
# Type: Bool
c.content.print_element_backgrounds = True

# Open new windows in private browsing mode which does not record
# visited pages.
# Type: Bool
c.content.private_browsing = True

# The proxy to use. In addition to the listed values, you can use a
# `socks://...` or `http://...` URL.
# Type: Proxy
# Valid values:
#   - system: Use the system wide proxy.
#   - none: Don't use any proxy
c.content.proxy = 'system'

# Send DNS requests over the configured proxy.
# Type: Bool
c.content.proxy_dns_requests = True

# Validate SSL handshakes.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
# c.content.ssl_strict = 'ask'

# A list of user stylesheet filenames to use.
# Type: List of File, or File
c.content.user_stylesheets = []

# Enables or disables WebGL.
# Type: Bool
c.content.webgl = True

# Whether load requests should be monitored for cross-site scripting
# attempts. Suspicious scripts will be blocked and reported in the
# inspector's JavaScript console. Enabling this feature might have an
# impact on performance.
# Type: Bool
c.content.xss_auditing = False

# The directory to save downloads to. If unset, a sensible os-specific
# default is used.
# Type: Directory
c.downloads.location.directory = '~/downloads'

# Prompt the user for the download location. If set to false,
# `downloads.location.directory` will be used.
# Type: Bool
c.downloads.location.prompt = True

# Remember the last used download directory.
# Type: Bool
c.downloads.location.remember = True

# What to display in the download filename input.
# Type: String
# Valid values:
#   - path: Show only the download path.
#   - filename: Show only download filename.
#   - both: Show download path and filename.
c.downloads.location.suggestion = 'path'

# The default program used to open downloads. If null, the default
# internal handler is used. Any `{}` in the string will be expanded to
# the filename, else the filename will be appended.
# Type: String
c.downloads.open_dispatcher = None

# Where to show the downloaded files.
# Type: VerticalPosition
# Valid values:
#   - top
#   - bottom
c.downloads.position = 'top'

# Number of milliseconds to wait before removing finished downloads. If
# set to -1, downloads are never removed.
# Type: Int
c.downloads.remove_finished = 1000

# The editor (and arguments) to use for the `open-editor` command. `{}`
# gets replaced by the filename of the file to be edited.
# Type: ShellCommand
c.editor.command = ['termite -c zsh -e nvim', '-f', '{}']

# Encoding to use for the editor.
# Type: Encoding
c.editor.encoding = 'utf-8'

# Controls when a hint can be automatically followed without pressing
# Enter.
# Type: String
# Valid values:
#   - always: Auto-follow whenever there is only a single hint on a page.
#   - unique-match: Auto-follow whenever there is a unique non-empty match in either the hint string (word mode) or filter (number mode).
#   - full-match: Follow the hint when the user typed the whole hint (letter, word or number mode) or the element's text (only in number mode).
#   - never: The user will always need to press Enter to follow a hint.
c.hints.auto_follow = 'unique-match'

# A timeout (in milliseconds) to ignore normal-mode key bindings after a
# successful auto-follow.
# Type: Int
c.hints.auto_follow_timeout = 0

# CSS border value for hints.
# Type: String
c.hints.border = '1px solid #E3BE23'

# Chars used for hint strings.
# Type: UniqueCharString
c.hints.chars = 'asdfghjkl'

# The dictionary file to be used by the word hints.
# Type: File
c.hints.dictionary = '/usr/share/dict/words'

# Which implementation to use to find elements to hint.
# Type: String
# Valid values:
#   - javascript: Better but slower
#   - python: Slightly worse but faster
c.hints.find_implementation = 'python'

# Hide unmatched hints in rapid mode.
# Type: Bool
c.hints.hide_unmatched_rapid_hints = True

# Minimum number of chars used for hint strings.
# Type: Int
c.hints.min_chars = 1

# Mode to use for hints.
# Type: String
# Valid values:
#   - number: Use numeric hints. (In this mode you can also type letters from the hinted element to filter and reduce the number of elements that are hinted.)
#   - letter: Use the chars in the `hints.chars` setting.
#   - word: Use hints words based on the html elements and the extra words.
c.hints.mode = 'letter'

# A comma-separated list of regexes to use for 'next' links.
# Type: List of Regex
c.hints.next_regexes = ['\\bnext\\b', '\\bmore\\b', '\\bnewer\\b', '\\b[>→≫]\\b', '\\b(>>|»)\\b', '\\bcontinue\\b']

# A comma-separated list of regexes to use for 'prev' links.
# Type: List of Regex
c.hints.prev_regexes = ['\\bprev(ious)?\\b', '\\bback\\b', '\\bolder\\b', '\\b[<←≪]\\b', '\\b(<<|«)\\b']

# Scatter hint key chains (like Vimium) or not (like dwb). Ignored for
# number hints.
# Type: Bool
c.hints.scatter = True

# Make chars in hint strings uppercase.
# Type: Bool
c.hints.uppercase = False

# The maximum time in minutes between two history items for them to be
# considered being from the same browsing session. Items with less time
# between them are grouped when being displayed in `:history`. Use -1 to
# disable separation.
# Type: Int
c.history_gap_interval = 30

# Find text on a page case-insensitively.
# Type: String
# Valid values:
#   - always: Search case-insensitively
#   - never: Search case-sensitively
#   - smart: Search case-sensitively if there are capital chars
c.search.ignore_case = 'smart'

# Forward unbound keys to the webview in normal mode.
# Type: String
# Valid values:
#   - all: Forward all unbound keys.
#   - auto: Forward unbound non-alphanumeric keys.
#   - none: Don't forward any keys.
c.input.forward_unbound_keys = 'auto'

# Leave insert mode if a non-editable element is clicked.
# Type: Bool
c.input.insert_mode.auto_leave = True

# Automatically enter insert mode if an editable element is focused
# after loading the page.
# Type: Bool
c.input.insert_mode.auto_load = False

# Switch to insert mode when clicking flash and other plugins.
# Type: Bool
c.input.insert_mode.plugins = False

# Include hyperlinks in the keyboard focus chain when tabbing.
# Type: Bool
c.input.links_included_in_focus_chain = True

# Timeout (in milliseconds) for partially typed key bindings. If the
# current input forms only partial matches, the keystring will be
# cleared after this time.
# Type: Int
c.input.partial_timeout = 5000

# Enable Spatial Navigation. Spatial navigation consists in the ability
# to navigate between focusable elements in a Web page, such as
# hyperlinks and form controls, by using Left, Right, Up and Down arrow
# keys. For example, if a user presses the Right key, heuristics
# determine whether there is an element he might be trying to reach
# towards the right and which element he probably wants.
# Type: Bool
c.input.spatial_navigation = False

# Keychains that shouldn't be shown in the keyhint dialog. Globs are
# supported, so `;*` will blacklist all keychains starting with `;`. Use
# `*` to disable keyhints.
# Type: List of String
c.keyhint.blacklist = []

# Time from pressing a key to seeing the keyhint dialog (ms).
# Type: Int
c.keyhint.delay = 500

# Time (in ms) to show messages in the statusbar for. Set to 0 to never
# clear messages.
# Type: Int
c.messages.timeout = 2000

# How to open links in an existing instance if a new one is launched.
# This happens when e.g. opening a link from a terminal. See
# `new_instance_open_target_window` to customize in which window the
# link is opened in.
# Type: String
# Valid values:
#   - tab: Open a new tab in the existing window and activate the window.
#   - tab-bg: Open a new background tab in the existing window and activate the window.
#   - tab-silent: Open a new tab in the existing window without activating the window.
#   - tab-bg-silent: Open a new background tab in the existing window without activating the window.
#   - window: Open in a new window.
c.new_instance_open_target = 'tab'

# Which window to choose when opening links as new tabs. When
# `new_instance_open_target` is not set to `window`, this is ignored.
# Type: String
# Valid values:
#   - first-opened: Open new tabs in the first (oldest) opened window.
#   - last-opened: Open new tabs in the last (newest) opened window.
#   - last-focused: Open new tabs in the most recently focused window.
#   - last-visible: Open new tabs in the most recently visible window.
c.new_instance_open_target_window = 'last-focused'

# Show a filebrowser in upload/download prompts.
# Type: Bool
c.prompt.filebrowser = True

# The rounding radius for the edges of prompts.
# Type: Int
c.prompt.radius = 8

# Additional arguments to pass to Qt, without leading `--`. With
# QtWebEngine, some Chromium arguments (see
# https://peter.sh/experiments/chromium-command-line-switches/ for a
# list) will work. This setting requires a restart.
# Type: List of String
c.qt.args = []

# Force a Qt platform to use. This sets the `QT_QPA_PLATFORM`
# environment variable and is useful to force using the XCB plugin when
# running QtWebEngine on Wayland.
# Type: String
c.qt.force_platform = None

# Force software rendering for QtWebEngine. This is needed for
# QtWebEngine to work with Nouveau drivers. This setting requires a
# restart.
# Type: Str
c.qt.force_software_rendering = 'software-opengl'

# Show a scrollbar.
# Type: Str
c.scrolling.bar = 'never'

# Enable smooth scrolling for web pages. Note smooth scrolling does not
# work with the `:scroll-px` command.
# Type: Bool
c.scrolling.smooth = False

# The name of the session to save by default. If this is set to null,
# the session which was last loaded is saved.
# Type: SessionName
c.session.default_name = None

# Spell checking languages. You can check for available languages and
# install dictionaries using scripts/install_dict.py. Run the script
# with -h/--help for instructions.
# Type: List of String
# Valid values:
#   - af-ZA: Afrikaans (South Africa)
#   - bg-BG: Bulgarian (Bulgaria)
#   - ca-ES: Catalan (Spain)
#   - cs-CZ: Czech (Czech Republic)
#   - da-DK: Danish (Denmark)
#   - de-DE: German (Germany)
#   - el-GR: Greek (Greece)
#   - en-CA: English (Canada)
#   - en-GB: English (United Kingdom)
#   - en-US: English (United States)
#   - es-ES: Spanish (Spain)
#   - et-EE: Estonian (Estonia)
#   - fa-IR: Farsi (Iran)
#   - fo-FO: Faroese (Faroe Islands)
#   - fr-FR: French (France)
#   - he-IL: Hebrew (Israel)
#   - hi-IN: Hindi (India)
#   - hr-HR: Croatian (Croatia)
#   - hu-HU: Hungarian (Hungary)
#   - id-ID: Indonesian (Indonesia)
#   - it-IT: Italian (Italy)
#   - ko: Korean
#   - lt-LT: Lithuanian (Lithuania)
#   - lv-LV: Latvian (Latvia)
#   - nb-NO: Norwegian (Norway)
#   - nl-NL: Dutch (Netherlands)
#   - pl-PL: Polish (Poland)
#   - pt-BR: Portuguese (Brazil)
#   - pt-PT: Portuguese (Portugal)
#   - ro-RO: Romanian (Romania)
#   - ru-RU: Russian (Russia)
#   - sh: Serbo-Croatian
#   - sk-SK: Slovak (Slovakia)
#   - sl-SI: Slovenian (Slovenia)
#   - sq: Albanian
#   - sr: Serbian
#   - sv-SE: Swedish (Sweden)
#   - ta-IN: Tamil (India)
#   - tg-TG: Tajik (Tajikistan)
#   - tr-TR: Turkish (Turkey)
#   - uk-UA: Ukrainian (Ukraine)
#   - vi-VN: Vietnamese (Viet Nam)
c.spellcheck.languages = []

# Padding for the statusbar.
# Type: Padding
c.statusbar.padding = {'top': 1, 'bottom': 1, 'left': 0, 'right': 0}

# The position of the status bar.
# Type: VerticalPosition
# Valid values:
#   - top
#   - bottom
c.statusbar.position = 'bottom'

# Open new tabs (middleclick/ctrl+click) in the background.
# Type: Bool
c.tabs.background = True

# On which mouse button to close tabs.
# Type: String
# Valid values:
#   - right: Close tabs on right-click.
#   - middle: Close tabs on middle-click.
#   - none: Don't close tabs using the mouse.
c.tabs.close_mouse_button = 'middle'

# Scaling for favicons in the tab bar. The tab size is unchanged, so big
# favicons also require extra `tabs.padding`.
# Type: Float
c.tabs.favicons.scale = 1.0

# Show favicons in the tab bar.
# Type: Str
c.tabs.favicons.show = 'always'

# Padding for tab indicators
# Type: Padding
c.tabs.indicator.padding = {'top': 2, 'bottom': 2, 'left': 0, 'right': 4}

# Behavior when the last tab is closed.
# Type: String
# Valid values:
#   - ignore: Don't do anything.
#   - blank: Load a blank page.
#   - startpage: Load the start page.
#   - default-page: Load the default page.
#   - close: Close the window.
c.tabs.last_close = 'close'

# Switch between tabs using the mouse wheel.
# Type: Bool
c.tabs.mousewheel_switching = True

# How new tabs opened from another tab are positioned.
# Type: NewTabPosition
# Valid values:
#   - prev: Before the current tab.
#   - next: After the current tab.
#   - first: At the beginning.
#   - last: At the end.
c.tabs.new_position.related = 'next'

# How new tabs which aren't opened from another tab are positioned.
# Type: NewTabPosition
# Valid values:
#   - prev: Before the current tab.
#   - next: After the current tab.
#   - first: At the beginning.
#   - last: At the end.
c.tabs.new_position.unrelated = 'last'

# Padding around text for tabs
# Type: Padding
c.tabs.padding = {'top': 0, 'bottom': 0, 'left': 5, 'right': 5}

# The position of the tab bar.
# Type: Position
# Valid values:
#   - top
#   - bottom
#   - left
#   - right
c.tabs.position = 'top'

# Which tab to select when the focused tab is removed.
# Type: SelectOnRemove
# Valid values:
#   - prev: Select the tab which came before the closed one (left in horizontal, above in vertical).
#   - next: Select the tab which came after the closed one (right in horizontal, below in vertical).
#   - last-used: Select the previously selected tab.
c.tabs.select_on_remove = 'next'

# When to show the tab bar.
# Type: String
# Valid values:
#   - always: Always show the tab bar.
#   - never: Always hide the tab bar.
#   - multiple: Hide the tab bar if only one tab is open.
#   - switching: Show the tab bar when switching tabs.
c.tabs.show = 'always'

# Time to show the tab bar before hiding it when tabs.show is set to
# 'switching'.
# Type: Int
c.tabs.show_switching_delay = 800

# Open a new window for every tab.
# Type: Bool
c.tabs.tabs_are_windows = False

# Alignment of the text inside of tabs.
# Type: TextAlignment
# Valid values:
#   - left
#   - right
#   - center
c.tabs.title.alignment = 'left'

# The format to use for the tab title. The following placeholders are
# defined:  * `{perc}`: The percentage as a string like `[10%]`. *
# `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
# the current web page * `{title_sep}`: The string ` - ` if a title is
# set, empty otherwise. * `{index}`: The index of this tab. * `{id}`:
# The internal tab ID of this tab. * `{scroll_pos}`: The page scroll
# position. * `{host}`: The host of the current web page. * `{backend}`:
# Either ''webkit'' or ''webengine'' * `{private}` : Indicates when
# private mode is enabled.
# Type: FormatString
c.tabs.title.format = '{index}: {current_title}'

# The format to use for the tab title for pinned tabs. The same
# placeholders like for `tabs.title.format` are defined.
# Type: FormatString
c.tabs.title.format_pinned = '{index}'

# The width of the tab bar if it's vertical, in px or as percentage of
# the window.
# Type: PercOrInt
c.tabs.width = '20%'

# Width of the progress indicator (0 to disable).
# Type: Int
c.tabs.width = 3

# Whether to wrap when changing tabs.
# Type: Bool
c.tabs.wrap = True

# Whether to start a search when something else than a URL is entered.
# Type: String
# Valid values:
#   - naive: Use simple/naive check.
#   - dns: Use DNS requests (might be slow!).
#   - never: Never search automatically.
c.url.auto_search = 'naive'

# The page to open if :open -t/-b/-w is used without URL. Use
# `about:blank` for a blank page.
# Type: FuzzyUrl
c.url.default_page = searchengine

# The URL segments where `:navigate increment/decrement` will search for
# a number.
# Type: FlagList
# Valid values:
#   - host
#   - path
#   - query
#   - anchor
c.url.incdec_segments = ['path', 'query']

# Definitions of search engines which can be used via the address bar.
# Maps a searchengine name (such as `DEFAULT`, or `ddg`) to a URL with a
# `{}` placeholder. The placeholder will be replaced by the search term,
# use `{{` and `}}` for literal `{`/`}` signs. The searchengine named
# `DEFAULT` is used when `url.auto_search` is turned on and something
# else than a URL was entered to be opened. Other search engines can be
# used by prepending the search engine name to the search term, e.g.
# `:open google qutebrowser`.
# Type: Dict
c.url.searchengines = {'DEFAULT': searchengine+'?q={}'}

# The page(s) to open at the start.
# Type: List of FuzzyUrl, or FuzzyUrl
c.url.start_pages = [searchengine]

# The URL parameters to strip with `:yank url`.
# Type: List of String
c.url.yank_ignored_parameters = ['ref', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content']

# Hide the window decoration when using wayland (requires restart)
# Type: Bool
c.window.hide_decoration = False

# The format to use for the window title. The following placeholders are
# defined:  * `{perc}`: The percentage as a string like `[10%]`. *
# `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
# the current web page * `{title_sep}`: The string ` - ` if a title is
# set, empty otherwise. * `{id}`: The internal window ID of this window.
# * `{scroll_pos}`: The page scroll position. * `{host}`: The host of
# the current web page. * `{backend}`: Either ''webkit'' or
# ''webengine'' * `{private}` : Indicates when private mode is enabled.
# Type: FormatString
c.window.title_format = '{perc}qutebrowser {title_sep} {current_title}'
