. ~/.config/dotfiles/config

export RANGER_LOAD_DEFAULT_RC=FALSE

export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

export XINITRC=~/.config/X11/xinitrc
export XSERVERRC=~/.config/X11/xserverrc

export ZDOTDIR=~/.config/zsh/
