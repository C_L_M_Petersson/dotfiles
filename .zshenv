. ~/.config/dotfiles/config

# XDG Base Directory {{{
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
# }}}

# XDG Config Home {{{
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
export XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc"

export XMONAD_CACHE_HOME="$XDG_CACHE_HOME/xmonad"
export XMONAD_CONFIG_HOME="$XDG_CONFIG_HOME/xmonad"
export XMONAD_DATA_HOME="$XDG_DATA_HOME/xmonad"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
# }}}

# Misc {{{
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

export RANGER_LOAD_DEFAULT_RC=FALSE
# }}}
